package admu.cybermejo.stk;

import android.app.Activity;
import android.os.Bundle;

public class AboutActivity extends Activity {
	
	/*
	 * About Activity
	 */
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
    }
}
