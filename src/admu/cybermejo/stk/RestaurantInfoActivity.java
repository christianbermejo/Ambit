package admu.cybermejo.stk;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.TextView;

public class RestaurantInfoActivity extends PreferenceActivity {
	
	/*
	 * Contains the RestaurantInfoActivity
	 * The Intent and Bundle is from GMaps Fragment
	 * Individual ContactPreferences are populated here,
	 * since they are only 3 so far.
	 * 
	 * Are there other ways to optimize the retrieval of data?
	 */

    @SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resto_info);
        addPreferencesFromResource(R.xml.contact_prefs);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        Log.i("BUNDLE-RESTOINFO", String.valueOf(extras));
        String website = extras.getString("website");
        String address = extras.getString("address");
        String phone = extras.getString("phone");
        String name = intent.getStringExtra("name");
        String vicinity = intent.getStringExtra("vicinity");
        double lat = extras.getDouble("lat", 0);
        double lng = extras.getDouble("lng", 0);
        String latlng = String.valueOf(lat) + "," + String.valueOf(lng);
        ContactPreference prefphone = (ContactPreference) findPreference("phone");
        ContactPreference prefweb = (ContactPreference) findPreference("web");
        ContactPreference prefmap = (ContactPreference) findPreference("maps");
        

        prefphone.setSummary(String.valueOf(phone));
        prefweb.setSummary(String.valueOf(website));
        prefmap.setSummary(latlng);

        TextView restoname = (TextView) findViewById(R.id.tv_restoinfoname);
        TextView restoaddress = (TextView) findViewById(R.id.tv_restoaddress);
        
        restoname.setText(name);
        restoaddress.setText(address);
    }
}

