package admu.cybermejo.stk;

import org.utils.Locations;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class GpsLocationReceiver extends BroadcastReceiver implements LocationListener{ 
	/*
	 * BroadcastReceiver used to listen for GPS Provider
	 * changes and requests location updates immediately
	 * for the application instance
	 */
	LocationManager lm;
	Ambit ambit = Ambit.getInstance();
	boolean gps_enabled  = false, network_enabled  = false;
	        @Override
	        public void onReceive(Context context, Intent intent) {
			ambit = Ambit.getInstance();
	            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
	            	lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	                try {
	                ambit.provider = Locations.getBestProvider();
	                lm.requestLocationUpdates(ambit.provider, 1000, 0, (LocationListener) this);
	                } catch (NullPointerException e) {
	                	Log.e("GPS PROVIDER OFF", "GPS has been turned off");
	                }
	                Log.d("PROVIDER CHANGE", "Provider changed to: " + String.valueOf(ambit.provider));
	            }
	        }

	        @Override
	        public void onLocationChanged(Location arg0) {
	        }

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onProviderDisabled(String provider) {
			}
}
