package admu.cybermejo.stk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ContactPreference extends Preference {
	
	/*
	 * For ContactPreference, contains also
	 * onClick actions for each key
	 */
	
	String prefSummary=null;
	
	private enum Pref {
	    phone, web, maps;
	}
	
	public ContactPreference(Context context, AttributeSet aSet) { 
		super(context, aSet); 
		}
	
	@Override
	protected View onCreateView( ViewGroup parent )
	{
	  LayoutInflater li = (LayoutInflater)getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
	  return li.inflate( R.layout.contact_preference, parent, false);
	}
	
	@Override
    protected void onClick() {
		String a = this.getKey();
		Pref pref = Pref.valueOf(a);
		
		switch(pref) {
	    case phone:
	    	prefSummary = (String) this.getSummary();
	    	if (prefSummary.isEmpty() || prefSummary.equalsIgnoreCase(null)) {
	    		Log.e("Empty String", a + " is empty");
	    	} else {
	    		Intent viewIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + prefSummary));
		    	setIntent(viewIntent);
	    	}
	        break;
	    case web:
	    	// Opens website if not empty
	    	prefSummary = (String) this.getSummary();
	    	if (prefSummary.isEmpty() || prefSummary.equalsIgnoreCase(null)) {
	    		Log.e("Empty String", a + " is empty");
	    	} else {
	    	Intent viewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(prefSummary));
	    	setIntent(viewIntent);
	    	}
	        break;
	    case maps:
	    	prefSummary = (String) this.getSummary();
	    	if (prefSummary.isEmpty() || prefSummary.equalsIgnoreCase(null)) {
	    		Log.e("Empty String", a + " is empty");
	    	} else {
	    	Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
	    	Uri.parse("http://maps.google.com/maps?f=d&daddr=" + prefSummary));
	    	setIntent(intent);
	    	}
	    	Log.i("EMAIL clicked", "Pref clicked: " + a);
	    	break;
	}
		
		
	}
}
