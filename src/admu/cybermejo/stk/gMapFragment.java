package admu.cybermejo.stk;

import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;
import org.utils.Locations;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class gMapFragment extends Fragment implements LocationListener{
	
	/*
	 * Contains the gMapFragment along
	 * with the bulk of the JSON retrieving and parsing code
	 * courtesy of Volley
	 * 
	 * A good to-do next would be to globalize the 
	 * JSONRequester and JSONParser. Also, optimize
	 * the AsyncTasks if possible
	 */
	GoogleMap gm;
	Context ctx;
	double mLatitude, mLongitude, lat, lng =0;
	double lat2, lng2;
	TextView RestoName, RestoAddress;
	public String name, vicinity, address, phone, website;
	public static final String TAG = Ambit.class.getSimpleName();
	public String reference="";
	
	Bundle b = new Bundle();
	Random rand ;
	StringBuilder sb;

	Bundle presult = new Bundle();
	
	Location loc;
	LocationManager lm;
	Ambit ambit;
	
	public gMapFragment() {
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View v;
		v = inflater.inflate(R.layout.frag_main_withconnect, container,
					false);
		// Initializing Maps
		ambit = (Ambit) getActivity().getApplicationContext();
		gm = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        gm.getUiSettings().setZoomControlsEnabled(false);
        gm.setMyLocationEnabled(true);
        ctx = getActivity().getApplicationContext();
        
        loc = Locations.setupLocation(ctx);
        ambit.provider = Locations.returnProvider();
        lm = Locations.returnLocationManager();
        if(loc!=null){
                onLocationChanged(loc);
        }
        lm.getLastKnownLocation(ambit.provider);
        lm.requestLocationUpdates(ambit.provider, 1000, 0, this);
        Log.e("PROVIDER", "Current Provider:" + String.valueOf(ambit.provider));

        if (Locations.checkForGPS(lm)) {
        	new AlertDialog.Builder(getActivity())
            .setMessage("I need to have GPS enabled in order to give you the best possible experience."
            		+ " Do you want to enable GPS?")
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) { 
                	Intent a = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(a);
                }
             })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) { 
                    // do nothing
                }
             })
             .show();
        } else {
        	
//        	b = makeJSONRequest(mLatitude, mLongitude, reference, 1);
        }
        
        RestoName = (TextView) v.findViewById(R.id.tv_restoname);
        RestoAddress = (TextView) v.findViewById(R.id.tv_restoaddress);
        
        Button yes = (Button) v.findViewById(R.id.btn_yes);
        yes.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
//    			new JSONRequester().execute(mLatitude,mLongitude,reference,2);	
    			Intent i = new Intent(ctx,RestaurantInfoActivity.class);
    			i.putExtra("name", name);
    			i.putExtra("vicinity", vicinity);
    			i.putExtra("lat", lat);
    			i.putExtra("lng", lng);
    			i.putExtra("currentLat", mLatitude);
    			i.putExtra("currentLng", mLongitude);
    			//TODO: add current LatLng
    			i.putExtras(presult); // should dump the address, phone and website Bundle with corresponding keys
    			// TODO: see if this interferes with the code
    			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    			ctx.startActivity(i);
    		}
    	});
        Button no = (Button) v.findViewById(R.id.btn_no);
        no.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			b = new Bundle();
    			new JSONRequester().execute(mLatitude,mLongitude,reference,1);
    		}
    	});
		return v;
	}
	
	public void requestCoordinates() {
		new JSONRequester().execute(mLatitude,mLongitude,reference,1);
	}

	public void onLocationChanged(Location location) {
		mLatitude = location.getLatitude();
		mLongitude = location.getLongitude();
		LatLng latLng = new LatLng(mLatitude, mLongitude);
		gm.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f), 4000, null);
		
	}
	
	private void parseBundle(Bundle b, int request) {
		switch(request) {
		case 1:
			 lat = b.getDouble("lat");
			 lng = b.getDouble("lng");
			 name = b.getString("name");
			 vicinity = b.getString("vicinity");
			 reference = b.getString("reference");
			 break;
		case 2:
			b.putAll(presult);
			Log.i("BundePutall", String.valueOf(presult));
			break;
		}
	}
	
	private void locateResult() {
		//Clears all the existing markers 
		 gm.clear();
		 // Creating a marker
		 MarkerOptions markerOptions = new MarkerOptions();
		 LatLng latLng = new LatLng(lat, lng); 
		 // Setting the position for the marker
		 markerOptions.position(latLng);
		 // Setting the title for the marker. 
		 //This will be displayed on taping the marker
		 markerOptions.title(name);
		 // Placing a marker on the touched position
		 gm.addMarker(markerOptions);
		 RestoName.setText(name);
		 RestoAddress.setText(vicinity);
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub	
	}
	
	private class JSONRequester extends AsyncTask<Object,Void,Boolean> {
		private final ProgressDialog dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		String ref;
		double mLat,mLong;
		int request;
		
		@Override
	    protected void onPreExecute() {       
	        super.onPreExecute();
	        dialog.setCancelable(false);
	        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
	        dialog.show();           
	    }
		
		@Override
		protected Boolean doInBackground(Object... params) {
			Log.i("BACKGROUND CHECK", "Passing through doInBackground");
			mLat = (Double) params [0];
			mLong = (Double) params [1];
			ref = (String) params [2];
			request = (Integer) params [3];
			try {
			makeJSONRequest(mLat, mLong, ref, request);
			} catch (Exception e) {
				return false;
			}
			try
             {
                 Thread.sleep(4000);
             } catch (InterruptedException e)
             {
                 e.printStackTrace();
             }
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean v){
			if (request == 2) {
				myHandler.sendEmptyMessage(1);
			} else {
			myHandler.sendEmptyMessage(0);
			}
//			parseBundle(v, request);
			dialog.dismiss();
		}
	}
		
    public Bundle makeJSONRequest(final double mLatitude, final double mLongitude, String ref, final int request) {
    	
    	rand = new Random();
    	
    	Log.e(TAG, "Case: " + request);
    	
    	final Bundle c = new Bundle();
    	
    	switch(request){
    	
    	case 1:
    		sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
    		sb.append("location="+mLatitude+","+mLongitude);
    		sb.append("&radius=2000");
    		sb.append("&types=cafe|restaurant|meal_takeaway|meal_delivery|food");
    		sb.append("&sensor=true");
    		sb.append("&pagetoken=");
    		sb.append("&key=AIzaSyDnRNKk1B6__uwOvtVJ7oAwWqzr3JNjB8M");
    		break;
    	case 2:
    		sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
    		sb.append("&sensor=true");
    		sb.append("&reference="+ref);
    		sb.append("&key=AIzaSyDnRNKk1B6__uwOvtVJ7oAwWqzr3JNjB8M");
    		break;
    	default:
    		break;
    	}
		// TODO: TOO MUCH PLACES API REQUESTS
		JsonObjectRequest req = new JsonObjectRequest(sb.toString(), null,
	            new Response.Listener<JSONObject>() {
	                @Override
	                public void onResponse(JSONObject response) {
	                    Log.d(TAG, response.toString());
	 
	                    try {
	                    	
	                    	//TODO: Should I break this down?
	                    	switch(request) {
	                    	case 1:
		                    	int randPlace = rand.nextInt(response.getJSONArray("results").length());
//		                    	location = response.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
		                        JSONObject location = response.getJSONArray("results").getJSONObject(randPlace);
	                        	
		                        JSONObject geomloc = location.getJSONObject("geometry").getJSONObject("location");
//		                        JSONObject loc = geom.getJSONObject("location");
		                        	
		                        lat = geomloc.getDouble("lat");
		                        lng = geomloc.getDouble("lng");
		                        name = location.getString("name");
		                        vicinity = location.getString("vicinity");
		                        reference = location.getString("reference");
	                    		//TODO FIX MESS AND SEE IT THERE ARE REDUNDANT CALLS
		                        c.putDouble("lat", lat);
		                        c.putDouble("lng", lng);
		                        c.putString("name", name);
		                        c.putString("vicinity", vicinity);
		                        c.putString("reference", reference);
		                        parseBundle(c, request);
		                    	locateResult();
		                        break;
	                    	case 2:
	                    		JSONObject placeresult = response.getJSONObject("result");
	                    		address = placeresult.optString("formatted_address", "");
	                    		phone = placeresult.optString("formatted_phone_number", "");
	                    		website = placeresult.optString("website", "");
	                    		//TODO FIX MESS AND SEE IT THERE ARE REDUNDANT CALLS
	                    		c.putString("address", address);
	                    		c.putString("phone",phone);
	                    		c.putString("website",website);
	                    		Log.i("BundePutall", String.valueOf(presult));
	                    		presult.putAll(c);
	                			Log.i("BundePutall", String.valueOf(presult));
	                    		break;
	                    	default:
	                    		break;
	                    	}
	                    } catch (JSONException e) {
	                        e.printStackTrace();
	                        Log.e(TAG, "Error: " + e.toString());
//	                        Toast.makeText(ctx,
//	                        		//TODO: Translate
//	                                "Oops, something went wrong. Try again!",
//	                                Toast.LENGTH_SHORT).show();
	                    } catch (IllegalArgumentException e) {
	                        e.printStackTrace();
	                        Log.d(TAG, "Possible provider mishap: passive, GPS, no cached location");
	                        Log.e(TAG, "Error: " + e.toString());
	                        Toast.makeText(ctx,
	                        		//TODO: Translate
	                                "Unable to find any results",
	                                Toast.LENGTH_SHORT).show();
	                    }
	                }
	            }, new Response.ErrorListener() {
	                @Override
	                public void onErrorResponse(VolleyError error) {
	                	Log.e(TAG, "Error: " + error.toString());
	                    Toast.makeText(ctx,
	                    		//TODO: Translate
	                           "Unable to contact server", Toast.LENGTH_SHORT).show();
	                }
	            });
        Ambit.getInstance().addToRequestQueue(req);
        return c;
    }
    
    final Handler myHandler = new Handler() {

	    @Override
	    public void handleMessage(Message msg) {
	        switch (msg.what) {
	        case 0:
	        	//TODO
	        	new JSONRequester().execute(mLatitude,mLongitude,reference,2);
	            break;
	            //TODO
	        case 1:
//    			Intent i = new Intent(ctx,RestaurantInfoActivity.class);
//    			i.putExtras(presult);
//    			i.putExtra("name", name);
//    			i.putExtra("vicinity", vicinity);
//    			i.putExtra("lat", lat);
//    			i.putExtra("lng", lng);
//    			 // should dump the address, phone and website Bundle with corresponding keys
//    			// TODO: see if this interferes with the code
//    			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//    			ctx.startActivity(i);
	        	break;
	        default:
	            break;
	        }
	    }
	};

}