package admu.cybermejo.stk;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager.BadTokenException;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	/*
	 * MainActivity which holds the 2 Fragments
	 * - GMapFragment
	 * - NoConnectionFragment
	 * - Also contains the materials for checking NetworkConnection
	 * 	- BroadcastReceiver, ASyncTask, and a public function
	 */
	
	boolean online;
	GoogleMap gm;
	ProgressDialog barProgressDialog;
	gMapFragment gMap;
	NoConnectFragment noConn;
	CheckConnectivity ccheck = null;
	Ambit ambit;
	public static MainActivity instance = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this; // instantiates activity for GpsLocationReceiver
     // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
        	int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }else {
        //TODO: Instantiates the 2 fragments and hides them
        	ambit = (Ambit) getApplicationContext();
        	gMap = new gMapFragment();
        	noConn = new NoConnectFragment();
        	getFragmentManager().beginTransaction()
 				.add (R.id.container, noConn, "noConn").hide(noConn)
 				.add (R.id.container, gMap, "gMap").hide(gMap).commit();
        }
      new CheckNetLoc().execute(MainActivity.this);
        
    }
    
    @Override
    protected void onPause() {
        unregisterReceiver(ccheck);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // CheckConnectivity BroadcastListener
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        ccheck = new CheckConnectivity();
        registerReceiver(ccheck, filter);
    }
    

    public class CheckNetLoc extends AsyncTask<Activity, Void, Boolean> {
    	
    	@Override
    	protected void onPreExecute() {
    	}
    	
		@Override
		protected Boolean doInBackground(Activity... activity) {
			return isNetworkAvailable(activity[0]);
		}
		protected void onPostExecute(Boolean result) {
			ambit.setConnection(result);
            Log.i("Internet Test", "Internet Test: " + String.valueOf(ambit.getConnection()));
           initUI();
		}
    }
        
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        		
        if (id == R.id.action_settings) {
        	Intent set = new Intent(getApplicationContext(), SettingsPageActivity.class);
	    	startActivity(set);
            return true;
        } else if (id == R.id.action_refresh) {
        	new CheckNetLoc().execute(MainActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void initUI() {
    	Fragment f = getFragmentManager().findFragmentById(R.id.container);
    	if (f != null && f.isVisible()) {
    		getFragmentManager().beginTransaction()
    		.hide(f)
    		.addToBackStack(null)
    		.commit();
    		
    	} else {
    	if (ambit.getConnection() == true) {
    		getFragmentManager().beginTransaction()
			.show(gMap).commit();
    		gMap.requestCoordinates();
    	} else if (ambit.getConnection() == false) {
    	Toast.makeText(this,"No Internet Connection found", Toast.LENGTH_SHORT).show();
    	getFragmentManager().beginTransaction()
			.show(noConn).commit();
    	}
    	}
    }
    
	public boolean isNetworkAvailable(Context ctx) {
	    ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo ni = cm.getActiveNetworkInfo();
	    return (ni != null && ni.isAvailable() && ni.isConnected());
	    // return true if available, false if not
	}
       
public class CheckConnectivity extends BroadcastReceiver{
    	@Override
    	public void onReceive(Context context, Intent arg1) {
    	    boolean isConnected = arg1.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
    	    if(isConnected){
    	        Log.i("INTERNET_LOST", "Internet Connection Lost");
    	    }
    	    else{
    	    	Log.i("INTERNET", "Internet Connected");
    	    }
    	    
    	   }
    	 }
}
