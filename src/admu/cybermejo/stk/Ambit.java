package admu.cybermejo.stk;

import org.utils.Locations;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Ambit extends Application {
	
	/*
	 * Application instantiation and global variables
	 * - Network Connectivity
	 * - Location Provider
	 * - Volley Instantiation
	 */
	
	 private boolean isOnline;
	 String provider = "";
	 
	 public void setProvider() {
		 this.provider = Locations.returnProvider();
	 }
	 
	 public String getProvider() {
		 return provider;
	 }
	
	 public boolean getConnection() {
	        return isOnline;
	    } 
	 public void setConnection(boolean isOnline) {
	        
	       this.isOnline = isOnline;      
	 }
	 public static final String TAG = Ambit.class.getSimpleName();
	  
	    private RequestQueue mRequestQueue;
	  
	    private static Ambit mInstance;
	  
	    @Override
	    public void onCreate() {
	        super.onCreate();
	        mInstance = this;
	    }
	  
	    public static synchronized Ambit getInstance() {
	        return mInstance;
	    }
	  
	    public RequestQueue getRequestQueue() {
	        if (mRequestQueue == null) {
	            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
	        }
	  
	        return mRequestQueue;
	    }
	  
	    public <T> void addToRequestQueue(Request<T> req, String tag) {
	        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
	        getRequestQueue().add(req);
	    }
	  
	    public <T> void addToRequestQueue(Request<T> req) {
	        req.setTag(TAG);
	        getRequestQueue().add(req);
	    }
	  
	    public void cancelPendingRequests(Object tag) {
	        if (mRequestQueue != null) {
	            mRequestQueue.cancelAll(tag);
	        }
	    }

}
