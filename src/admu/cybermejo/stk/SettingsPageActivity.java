package admu.cybermejo.stk;

import android.app.ActionBar;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingsPageActivity extends PreferenceActivity {
	
	/*
	 * Code for the Settings Page. ActionBar code should not be
	 * needed since style is set to fullscreen at manifest
	 */
	 @SuppressWarnings("deprecation")
	@Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        addPreferencesFromResource(R.xml.settings_prefs);
	        ActionBar ab = getActionBar();
	        ab.setDisplayShowTitleEnabled(false);
	    }
}
