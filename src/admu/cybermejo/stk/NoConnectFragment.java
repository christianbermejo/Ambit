package admu.cybermejo.stk;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class NoConnectFragment extends Fragment {
	
	/*
	 * Contains the NoConnectFragment
	 * Button behavior is behind in XML.
	 */
	
	public NoConnectFragment() {
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View v;
		v = inflater.inflate(R.layout.frag_main_noconnect, container,false);    
		return v;
	}
	
}