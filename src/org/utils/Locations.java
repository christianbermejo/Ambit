package org.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

public class Locations {
	
	/*
	 * Setting up Location and Provider for the Map
	 */
	
	static LocationManager locationManager;
	static String provider;
	static Location location;
	
	public static Location setupLocation(Context ctx) {
		// Getting LocationManager object from System Service LOCATION_SERVICE
        locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();
        // Getting the name of the best provider
        provider = locationManager.getBestProvider(criteria, true);
        // Getting Current Location From GPS
        location = locationManager.getLastKnownLocation(provider);
        return location;
	}
	
	public static String getBestProvider() {
		Criteria criteria = new Criteria();
		return provider = locationManager.getBestProvider(criteria, true);
	}
	
	public static String returnProvider() {
		return provider;
	}
	public static LocationManager returnLocationManager() {
		return locationManager;
	}
	
	public static boolean checkForGPS(LocationManager lm) {
		 boolean isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
	        boolean isNetworkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	        return (!isGPSEnabled && !isNetworkEnabled);  
	}

}
